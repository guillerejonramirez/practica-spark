import org.omg.SendingContext.RunTime;

/**
 * Created by guille on 25/04/2016.
 */
public class TwitterTrendingTopics {
    public static void main(String[] args){
        if(args.length==0){
            throw new RuntimeException("Thenumber of args is 0. Usage: "+ "TwitterTrendingTopics fileOrDirectoryName");
        }
    }

    SparkConf conf =new SparkConf().setAppName("TwitterTrendingTopics");

    JavaSparkContext sparkContext=new JavaSparkContext(conf);

    JavaRDD<String> lines=sparkContext.textFile(args[0]);

    JavaRDD<String> messages = lines.map(new Function<String, String>(){
        public String call(String s) {
            String [] split= s.toString().split("\t+");
            return split[2];
        }
    });

    JavaRDD<String> words =messages.flatMap(
            new FlatMapFunction<String, String>(){
                public Iterable call(String s) throws Exception {
                    return Arrays.asList(s.split(" "));
                }
            });

    JavaPairRDD<String, Integer> ones=words.mapToPair(
            new PairFunction<String, String, String, Integer>() {
                public Tuple2<String, Integer> call(String string) {
                    return new Tuple2<String, Integer>(string, 1);
                }
            }
    );

    JavaPairRDD<String, Integer> counts=ones.reduceByKey(
            new Function2<Integer, Integer, Integer>(){
                public Integer call(Integer integer, Integer integer2) throws  Exception{
                    return integer+integer2;
                }
            }
    );

    reverse.sortByKey().saveAsTextFile("C:\\");

    reverseContext.stop();
}

